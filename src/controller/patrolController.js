const express = require('express');
const router = express.Router();

const Patrol = require('../models/patrol'); 

router.post('/register', async (req, res) => {
    const { IP_Mobile, date  } = req.body;
    
    try {
        if (await Patrol.findOne({ IP_Mobile, date }))
            return res.status(400).send({ error: 'Register already exists' });
  
        const patrol = await Patrol.create(req.body);
  
        return res.send({ patrol });

    } catch (err) {
        return res.status(400).send({ error: 'Registration failed',err });
    }
});

router.get('/', async (req, res) => {
    try {
        Patrols = await Patrol.find();
  
        return res.send({ Patrols });
    } catch (err) {
        return res.status(400).send({ error: 'Error loading patrols' });
    }
});

module.exports = app => app.use('/getAllPatrol', router);