const express = require('express');
const bodyParser = require('body-parser');
var PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false}));

app.get('/', (req,res)=>{
    res.send('OK');
});

require('./controller/authController')(app);
require('./controller/projectController')(app);
require('./controller/patrolController')(app);

app.listen(PORT);
