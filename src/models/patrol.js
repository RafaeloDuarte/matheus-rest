const uuid = require('uuid');
const mongoose = require('../database/index.js');
const moment = require('moment');

function dataAtualFormatada(){
    var data = new Date(),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

const PatrolSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    IP_Mobile: {
        type: String,
        require:true,
        lowercase:true,
    },
    local: {
        type: String,
        require:true,
        lowercase:true,
    },
    checked: {
        type: String,
        require:true,
        lowercase:true,
    },
    UUID:{
        type: String,
        default:uuid,
    },
    date: {
        type: String,
        require:true,
            //Date.now.toString
    }
});


const Patrol = mongoose.model('Patrol', PatrolSchema);

module.exports = Patrol;